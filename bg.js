browser.browserAction.onClicked.addListener((tab) => {
    browser.tabs.query({currentWindow: true}).then( (list) => {
        yt_tabs = list.filter( it => it.url.match(/.*youtu.*be.*/) );

        yt_tabs.map( (it) => {
            browser.tabs.executeScript(it.id, {
                code: `var video = document.querySelector("video"); if (video.paused){video.play();} else {video.pause();}`
            });
        })
    });
})
